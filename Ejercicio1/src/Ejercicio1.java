import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio1 {
    /*
     * TODO Se deben capturar las excepciones que puedan
     * ocurrir y avisar al usuario. 
     *  
     * Lo que se indicó en clase es que no iba a capturarlas
     * para que nos centráramos en lo importante en la clase.
     * 
     * De hecho, sin capturar las excepciones no se puede 
     * mostrar un mensaje de error si no se puede ejecutar
     * el hijo
     * 
     */
    public static void main(String args[]) throws IOException, InterruptedException {

        String command = "ls";
        List<String> params = Arrays.asList(command.split(" "));
        ProcessBuilder pb = new ProcessBuilder(params);
        Process ls = pb.start();

        OutputStream os = ls.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bfw = new BufferedWriter(osw);

        Scanner sc = new Scanner(System.in);

        /**
         * ¿Qué finalidad tiene esta línea?
         */
        bfw.write(sc.nextLine());
        bfw.newLine();

        /**
         * Variable sin utilizar  
         */
        InputStream is;

        int resultado = ls.waitFor();

        if (resultado == 0) {
            is = ls.getInputStream();
        } else {
            is = ls.getErrorStream();
            /**
             * Aquí no se debería salir con System.exit(0)
             * porque da un error, habría que salir con algo
             * distinto. 
             * 
             * La línea de después del exit puede que no
             * llegue a ejecutarse
             */
            System.exit(0);
            System.out.println("Error");
        }

        while (sc.hasNext()) {
            if (resultado == 0) {
                System.out.println(sc.nextLine());
            } else {
                /**
                 * No se ha guardado la salida en un fichero
                 * Sólo se ha mostrado por pantalla
                 */
                System.out.println(sc.nextLine());
            }

        }
    }
}